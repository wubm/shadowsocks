#!/bin/bash
DIR_PATH=$(dirname $(readlink -f $0))

main() {
    : ${PASSWORD_FILE="/password.conf"}
    PASSWORD=$(cat $PASSWORD_FILE)

    echo "$(date +'[%d/%b/%Y %T]') Change password" >> /shadowsocks.log
    sed -i "s/password\":.*/password\": \"$PASSWORD\",/g" /etc/shadowsocks/config.json

    echo "$(date +'[%d/%b/%Y %T]') Start Service" >> /shadowsocks.log
    exec /usr/bin/python /usr/bin/ssserver -q -c /etc/shadowsocks/config.json
}

main "$@"
