FROM ubuntu:16.04
MAINTAINER wubm

RUN apt-get -y update; apt-get -y install shadowsocks
RUN sed -i "s/server\":.*/server\": \"0.0.0.0\",/g" /etc/shadowsocks/config.json

ENV USER=shadowsocks TERM=xterm
ENV HOME=/home/$USER
WORKDIR /

RUN useradd $USER
RUN chown -R $USER:$USER /etc/shadowsocks
RUN touch /shadowsocks.log && chown -R $USER:$USER /shadowsocks.log

ADD ./docker-entrypoint.sh /entrypoint.sh

EXPOSE 8388/tcp
EXPOSE 8388/udp

USER $USER

ENTRYPOINT ["/entrypoint.sh"]
